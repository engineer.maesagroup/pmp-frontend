@extends('admin.layouts.app')

@section('content')
    <form role="form" action="{{ route('update.gambar', $data->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class=" modal-body">
            <div class="form-group">
                <label for="exampleFormControlInput1">Slide</label>
                <select name="name" id="" class="form-control" required>
                    <option value="">Pilih Slide</option>
                    <option value="home">Home</option>
                    <option value="tentang">Tentang Kami</option>
                    <option value="karir">Karir</option>
                    <option value="mitra">Mitra dan Jarigan</option>
                    <option value="hubungan">Hubungan dan Investor</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Foto</label>
                <input class="form-control" value="{{ asset('storage/profile/' . $data->gambar) }}" name="gambar"
                    type="file">
                <img id="thumbnil" style="width:20%; margin-top:10px;"
                    src="{{ asset('storage/profile/' . $data->gambar) }}" alt="image" />
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="submit">Simpan</button>
        </div>
    </form>
@endsection

@section('addJs')
    <script>
        function showMyImage(fileInput) {
            var files = fileInput.files;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var imageType = /image.*/;
                if (!file.type.match(imageType)) {
                    continue;
                }
                var img = document.getElementById("thumbnil");
                img.file = file;
                var reader = new FileReader();
                reader.onload = (function(aImg) {
                    return function(e) {
                        aImg.src = e.target.result;
                    };
                })(img);
                reader.readAsDataURL(file);
            }
        }

    </script>
@endsection
