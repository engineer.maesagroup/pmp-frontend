<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public static function slide($image, $name, $oldName = null): void
    {
        if ($oldName) {
            Storage::delete('public/profile/' . $oldName);
        }
        Storage::putFileAs("public/profile", $image, $name);
    }
}
