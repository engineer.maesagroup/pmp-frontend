<?php

namespace App\Http\Controllers;

use App\Gambar;
use Illuminate\Http\Request;

class KarirController extends Controller
{
    public function index()
    {
        $data['data'] = Gambar::where('name', 'karir')->get();
        return view('karir', $data);
    }
    public function internship()
    {
        $data['data'] = Gambar::where('name', 'karir')->get();

        return view('internship', $data);
    }
    public function position_a()
    {
        $data['data'] = Gambar::where('name', 'karir')->get();

        return view('position_a', $data);
    }
    public function position_b()
    {
        $data['data'] = Gambar::where('name', 'karir')->get();
        return view('position_b', $data);
    }
}
