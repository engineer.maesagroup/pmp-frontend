<?php

namespace App\Http\Controllers;

use App\Gambar;
use Illuminate\Http\Request;

class MitraController extends Controller
{
    public function mitra()
    {
        $data['data'] = Gambar::where('name', 'mitra')->get();
        return view('mitra', $data);
    }
}
