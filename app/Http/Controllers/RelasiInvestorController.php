<?php

namespace App\Http\Controllers;

use App\Gambar;
use Illuminate\Http\Request;

class RelasiInvestorController extends Controller
{
    public function index()
    {
        $data['data'] = Gambar::where('name', 'hubungan')->get();
        return view('relasi_investor', $data);
    }
}
