<?php

namespace App\Http\Controllers;

use App\Gambar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GambarController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['data'] = Gambar::all();

        return view('admin.gambar', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'gambar' => 'required',
            'name' => 'required',
        ]);

        $image = new Gambar();
        if ($request->has("gambar")) {
            $imageName = Str::uuid();
            ImageController::slide($request->file("gambar"), $imageName, $image->gambar);
            $image->gambar = $imageName;
        }
        $image->name = $request->name;
        // dd($image);
        $image->save();
        Session::flash('tambah', 'Gambar telah berhasil di tambahkan');
        return redirect()->route('gambar');
    }

    public function edit($id)
    {
        $data['data'] = Gambar::findOrFail($id);
        return view('admin.gambar-edit', $data);
    }

    public function update(Request $request, $id)
    {
        $gambar = Gambar::findOrFail($id);

        $gambar->name = $request->name;
        if ($request->hasFile("gambar")) {
            $imageName = Str::uuid();
            ImageController::slide($request->file("gambar"), $imageName, $gambar->gambar);
            $gambar->gambar = $imageName;
        }
        $gambar->save();

        return redirect()->route('gambar');
    }
    public function destroy($id)
    {
        $gambar = Gambar::findOrFail($id);
        $gambar->delete();
        return redirect()->back();
    }
}
