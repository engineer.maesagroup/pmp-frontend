<?php

namespace App\Http\Controllers;

use App\Berita;
use App\Gambar;
use Barryvdh\DomPDF\PDF as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class TentangKamiController extends Controller
{
    public function index(Berita $berita)
    {
        $berita = Berita::latest()->paginate(4);
        $gambar = Gambar::where('name', 'tentang')->get();
        $data = [
            'data' => $berita,
            'gambar' => $gambar,
        ];

        // dd($data);
        return view('tentang_kami', $data);
    }

    public function generatePDF()

    {
        $data = ['title' => 'Welcome to belajarphp.net'];

        $pdf = PDF::loadView('PDF', $data);
        return $pdf->stream();
    }

    public function sejarah()
    {
        return view('sejarahkami');
    }

    public function details($id)
    {
        $id = base64_decode($id);
        $berita['berita'] = Berita::findOrFail($id);
        return view('berita', $berita);
    }
}
