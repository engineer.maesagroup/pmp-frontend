<?php

namespace App\Http\Controllers;

use App\Gambar;

class ProdukController extends Controller
{
    public function index()
    {
        $data['data'] = Gambar::where('name', 'home')->get();
        return view('produk_balok', $data);
    }

    public function index1()
    {
        $data['data'] = Gambar::where('name', 'home')->get();
        return view('produk_kristal', $data);
    }
}
